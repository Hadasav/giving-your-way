﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Giving_Your_Way
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


			routes.MapRoute(
			name: "Landing",
			url: "Landing",
			defaults: new { controller = "Landing", action = "Index" }
		);

			routes.MapRoute(
				name: "catalogs/index",
				url: "catalogs/index",
				defaults: new { controller = "Catalog", action = "Index" }
			);
			routes.MapRoute(
				name: "post-login",
				url: "post-login",
				defaults: new { controller = "Landing", action = "Organizations", mobile = UrlParameter.Optional }
			);

			routes.MapRoute(
				name: "organizations",
				url: "Landing/organizations",
				defaults: new { controller = "Landing", action = "Organizations", mobile = UrlParameter.Optional }
			);

			

			//routes.MapRoute(
			//	name: "Default",
			//	url: "{controller}/{action}/{id}",
			//	defaults: new { controller = "Landing", action = "Index", id = UrlParameter.Optional }
			//);
		}
	}
}