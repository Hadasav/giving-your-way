﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Model;
using Domain.Services;
using Giving_Your_Way.Models;

namespace Giving_Your_Way.Controllers
{
	public class CatalogController : Controller
	{
		private readonly IPlatformApi _IPlatformApi;

		public CatalogController(IPlatformApi IPlatformApi)
		{
			_IPlatformApi = IPlatformApi;
		}
		//
		// GET: /Catalog/

		public ActionResult Index(string token, int? catalogId)
		{
			var organizationId = "5740405";
			var catalogsIds = new List<int> { 9951104, 9951126 };
			var catalogs = _IPlatformApi.GetCatalogs(catalogsIds, token);
			List<UserModel> organization = _IPlatformApi.GetUserProfileData(organizationId, token);
			var catalogsViewModel = InitCatalogs(catalogs, organization.FirstOrDefault());
			return View(catalogsViewModel);
		}

		private OrganizationCatalogsViewModel InitCatalogs(List<CatalogModel> catalogs, UserModel organization)
		{
			var organizationCatalogs = new OrganizationCatalogsViewModel();
			organizationCatalogs.Catalogs = new List<CatalogViewModel>();
			foreach (var catalogModel in catalogs)
			{
				var catalog = new CatalogViewModel();
				catalog.CatalogDescription = catalogModel.Description;
				catalog.CatalogName = catalogModel.Name;
				catalog.CatalogId = catalogModel.Id;
				
				organizationCatalogs.Catalogs.Add(catalog);
			}
			organizationCatalogs.CatalogOwnerDetails = new UserViewModel();
			organizationCatalogs.CatalogOwnerDetails.Address = organization.AboutMe;
			//catalog.CatalogOwnerDetails.About = "catalog Owner About";
			organizationCatalogs.CatalogOwnerDetails.ImageUrl =
				"http://sandboxstatic1.shopyourway.com/getImage?url=http%3a%2f%2fc.shld.net%2frpx%2fi%2fs%2fpi%2fmp%2f19845%2f3743179508%3fsrc%3dhttp%253A%252F%252Fecx.images-amazon.com%252Fimages%252FI%252F31Ih3o3Q1ML.jpg%26d%3d1d2ffe6cd15e4335b54abb30c87678625d6e3395&t=Catalog&w=100&h=100&qlt=75&mrg=1&s=4db9c1043e4aee6af7409c308564e75d";
			organizationCatalogs.CatalogOwnerDetails.Name = organization.FullName.FirstName + " " + organization.FullName.LastName;
			organizationCatalogs.CatalogOwnerDetails.Id = organization.UserId;
			return organizationCatalogs;

		}

	}
}
