﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Domain.Model;
using Domain.Services;
using Giving_Your_Way.Models;

namespace Giving_Your_Way.Controllers
{
    public class LandingController : Controller
    {
		private readonly IPlatformApi _IPlatformApi;

		public LandingController(IPlatformApi IPlatformApi)
		{
			_IPlatformApi = IPlatformApi;
		}
        //
        // GET: /Landing/

        public RedirectToRouteResult Index()
        {
			return RedirectToRoute("catalogs/index", new { token = Request.Params["token"] }); //Action( "Index"); //, new { mobile = true }

	        //return "ok";
        }
		public ActionResult Organizations(string token)
		{
			var organizationsId = "5740405,5740389,5740388,5740412,5740387,5740410,5740413";
			List<UserModel> organizations = _IPlatformApi.GetUserProfileData(organizationsId, Request.Params["token"]);
			var organizationsViewModel = InitOrganizations(organizations);
			return View(organizationsViewModel);
		}

		private List<UserViewModel> InitOrganizations(List<UserModel> organizations)
		{
			var organizationsList = new List<UserViewModel>();
			foreach (var organization in organizations)
			{
				var organizationModel = new UserViewModel();
				organizationModel.Address = organization.AboutMe;
				//catalog.CatalogOwnerDetails.About = "catalog Owner About";
				organizationModel.ImageUrl =
					"http://sandboxstatic1.shopyourway.com/getImage?url=http%3a%2f%2fc.shld.net%2frpx%2fi%2fs%2fpi%2fmp%2f19845%2f3743179508%3fsrc%3dhttp%253A%252F%252Fecx.images-amazon.com%252Fimages%252FI%252F31Ih3o3Q1ML.jpg%26d%3d1d2ffe6cd15e4335b54abb30c87678625d6e3395&t=Catalog&w=100&h=100&qlt=75&mrg=1&s=4db9c1043e4aee6af7409c308564e75d";
				organizationModel.Name = organization.FullName.FirstName + " " +
				                                                organization.FullName.LastName;
				organizationModel.Id = organization.UserId;
				organizationsList.Add(organizationModel);
			}
			return organizationsList;
		}

    }
}
