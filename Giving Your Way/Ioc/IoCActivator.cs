﻿using System.Web.Http;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Domain.Ioc;
using Giving_Your_Way.Ioc;
using Mongo.Ioc;


[assembly: WebActivatorEx.PreApplicationStartMethod(type: typeof(IoCActivator), methodName: "Start")]
namespace Giving_Your_Way.Ioc
{
	public class IoCActivator
	{
		public static void Start()
		{
			var container = new WindsorContainer()
				.Install(FromAssembly.This())
				.Install(FromAssembly.Containing<MongoInstaller>())
				.Install(FromAssembly.Containing<DomainInstaller>());


			var mvcControllerfactory = new WindsorControllerFactory(container.Kernel);
			ControllerBuilder.Current.SetControllerFactory(mvcControllerfactory);

			var dependencyResolver = new WindsorDependencyResolver(container.Kernel);

			GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;

			DependencyResolver.SetResolver(dependencyResolver);
		}
	}
}