﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Giving_Your_Way.Models
{
	public class UserViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public string About { get; set; }
		public string Address { get; set; }

	}
}