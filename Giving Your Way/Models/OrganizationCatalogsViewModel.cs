﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Giving_Your_Way.Models
{
	public class OrganizationCatalogsViewModel
	{
		public List<CatalogViewModel> Catalogs { get; set; }
		public UserViewModel CatalogOwnerDetails { get; set; }
	}
}