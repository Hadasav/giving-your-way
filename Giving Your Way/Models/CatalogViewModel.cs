﻿using System.Collections.Generic;

namespace Giving_Your_Way.Models
{
	public class CatalogViewModel
	{
		public int CatalogId { get; set; }
		public string CatalogName{ get; set; }
		public string CatalogDescription { get; set; }
		
	}
}