using System.Configuration;
using MongoDB.Driver;

namespace Mongo
{
	public interface IMongoServerProvider
	{
		//MongoServer Server { get; }
		//MongoConnectionStringBuilder Connection { get; }

		MongoClient Client { get; }
		IMongoDatabase DB { get; }
	}

	public class MongoServerProvider : IMongoServerProvider
	{
		public MongoServerProvider()
		{
			Client = new MongoClient("mongodb://localhost:27017");
			var db = Client.GetDatabase("GivingYourWay");
			DB = db;
			//Server = MongoServer.Create(Connection);
		}

		//public MongoServer Server { get; private set; }

		public MongoClient Client { get; private set; }
		public IMongoDatabase DB { get; private set; }


		//public MongoConnectionStringBuilder Connection
		//{
		//	get { return new MongoConnectionStringBuilder(ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString); }
		//}
	}
}