using MongoDB.Driver;

namespace Mongo
{
	public interface IMongoDatabaseProvider
	{
		IMongoDatabase  Create();
	}

	public class MongoDatabaseProvider : IMongoDatabaseProvider
	{
		private readonly IMongoServerProvider _serverProvider;

		public MongoDatabaseProvider(IMongoServerProvider serverProvider)
		{
			_serverProvider = serverProvider;
		}

		public IMongoDatabase Create()
		{
			//var server = _serverProvider.Server;
			//return server.GetDatabase(_serverProvider.Connection.DatabaseName);
			var client = _serverProvider.Client;
			return _serverProvider.DB;
		}
	}
}