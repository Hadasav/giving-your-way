using MongoDB.Bson;
using MongoDB.Driver;
//using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;

namespace Mongo
{
	public interface IMongoStorage<T> where T : class
	{
	//	void Save(T t);
	//	void Update(T t);
	//	IList<T> GetAll();
	//	IQueryable<T> Query();
	//	//MongoCollection<T> GetMongoCollection();
	////	MongoCollection<T> GetMongoCollection();
	//	IList<TC> SelectAs<TC>(IMongoQuery query, string[] fields) where TC : class;
	//	IList<T> RunFindCommand(string json);
	}

	public class MongoStorage<T> : IMongoStorage<T> where T : class
	{
		private readonly IMongoDatabaseProvider _mongoDatabaseProvider;

		public MongoStorage(IMongoDatabaseProvider mongoDatabaseProvider)
		{
			_mongoDatabaseProvider = mongoDatabaseProvider;
		}

		public void Save(T t)
		{
			//var collection = GetMongoCollection();

			//collection.Save(t);
		}

		public void Update(T t)
		{
		//	var collection = GetMongoCollection();

			//collection.Save(t);
		}

		//public IList<T> GetAll()
	//	{
		//	var collection = _mongoDatabaseProvider.Create().GetCollection<T>();
		//	return collection;
		//	return GetMongoCollection().FindAll().ToList();
		//}

		//public IQueryable<T> Query()
		//{
		//	return GetMongoCollection().AsQueryable();
		//}

		//public MongoCollection<T> GetMongoCollection()
		//{
		//	return _mongoDatabaseProvider.Create().GetCollection<T>(TableName());
		//}

		//public IList<TC> SelectAs<TC>(IMongoQuery query, string[] fields) where TC : class
		//{
		//	var result = GetMongoCollection().FindAs<TC>(query).SetFields(Fields.Include(fields)).ToList();

		//	return result;
		//}

		//public IList<T> RunFindCommand(string json)
		//{
		//	var doc = BsonDocument.Parse(json);
		////	var query = new QueryDocument(doc);
		////	return GetMongoCollection().Find(query).ToList();
		//}
		
		private static string TableName()
		{
			return Inflector.Pluralize((typeof(T).Name.ToLower().Replace("dto", string.Empty)));
		}
	}
}