﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Domain.Ioc
{
	public class DomainInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(
				Classes.FromThisAssembly().Pick()
					.WithService.DefaultInterfaces()
					.Configure(s => s.LifestyleSingleton()));
			container.Kernel.Resolver.AddSubResolver(new ArrayResolver(container.Kernel, true));
		}
	}
}
