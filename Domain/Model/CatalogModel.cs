﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
	public class CatalogModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int OwnerId { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdatedDate { get; set; }
		public string Url { get; set; }
		public string CoverImageUrl { get; set; }
		public string Privacy { get; set; }
		public int ItemsCount { get; set; }
		//items
		public string Type { get; set; }
		public int Likes { get; set; }
		public int Followers { get; set; }


	}
}
