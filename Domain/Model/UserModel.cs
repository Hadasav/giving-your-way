﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
	public class UserModel
	{
		public int UserId { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public string AboutMe { get; set; }
		public string Address { get; set; }
		public FullName FullName { get; set; }
	}

	public class FullName
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
