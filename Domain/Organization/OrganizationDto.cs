﻿using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Organization
{
    public class OrganizationDto
    {
	    [BsonId]
	    public long OrganizationId { get; set; }

	    public string OrganizationName { get; set; }
    }
}
