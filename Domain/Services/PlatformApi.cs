﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Domain.Model;
using Domain.Platform;

namespace Domain.Services
{
	public interface IPlatformApi
	{
		List<CatalogModel> GetCatalogs(List<int> catalogIdsInts, string token);
		List<UserModel> GetUserProfileData(string userId, string token);
	}

	public class PlatformApi : IPlatformApi
	{
		private readonly IPlatformSettings _platformSettings;

		public PlatformApi(IPlatformSettings platformSettings)
		{
			_platformSettings = platformSettings;
		}

		public List<CatalogModel> GetCatalogs(List<int> catalogIdsInts, string token)
		{
			var response = GetApiresponse(GetCatalogsApi(catalogIdsInts, token));
			JavaScriptSerializer json_serializer = new JavaScriptSerializer();
			var catalogModel = json_serializer.Deserialize<List<CatalogModel>>(response);
			return catalogModel;
		}


		private string GetCatalogsApi(List<int> catalogIdsInts, string token)
		{
			var catalogs = "";
			foreach (var id in catalogIdsInts)
			{
				catalogs += id + ",";
			}
			var hash = GetHash(token, _platformSettings.AppSecret);
			var currentUserUrl = string.Format("http://sandboxplatform.shopyourway.com/catalogs/get?token={0}&hash={1}&ids={2}", token, hash, catalogs);
			return currentUserUrl;
		}

		public List<UserModel> GetUserProfileData(string userId, string token)
		{
			var response = GetApiresponse(GetUserProfileDataApi(userId, token));
			JavaScriptSerializer json_serializer = new JavaScriptSerializer();
			var userModel = json_serializer.Deserialize<List<UserModel>>(response);
			return userModel;
		}

		private string GetUserProfileDataApi(string userId, string token)
		{
			var hash = GetHash(token, _platformSettings.AppSecret);
			var currentUserUrl = string.Format("http://sandboxplatform.shopyourway.com/users/profile/get?token={0}&hash={1}&userIds={2}&attributes=aboutme,fullname,address", token, hash, userId);
			return currentUserUrl;
		}

		private string GetApiresponse(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Stream dataStream = response.GetResponseStream();
			StreamReader reader = new StreamReader(dataStream);
			var responseFromServer = reader.ReadToEnd();
			return responseFromServer;
		}

		private string GetHash(string token, string secret)
		{
			var saltedWithSecret = Encoding.UTF8.GetBytes(token + secret);
			return SHA256.Create().ComputeHash(saltedWithSecret).ToHexString().ToLower();
		}
	}

	public static class ArrayExtensions
	{
		public static string ToHexString(this byte[] bytes)
		{
			return bytes.Aggregate(new StringBuilder(bytes.Length * 2), (sb, i) => sb.Append(i.ToString("x2"))).ToString();
		}
	}
}
