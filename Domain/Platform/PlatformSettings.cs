﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Platform
{
	public interface IPlatformSettings
	{
		string AppSecret { get; }
		int AppId { get; }
		Uri PlatformApiBaseUrl { get; }
		//	Uri PlatformSecureApiBaseUrl { get; }
	}

	public class PlatformSettings : IPlatformSettings
	{
		//[Default("564b010436be48088f85a2e9ba585f0d")]
		public string AppSecret
		{
			get { return ConfigurationManager.AppSettings["AppSecret"]; }
		}

		//	[Default(6525)]
		public int AppId { get { return Int32.Parse(ConfigurationManager.AppSettings["AppID"]); } }

		//	[Default("0_6525_253402300799_1_f20ab9b60aeef88568d965f255e4530393a27aa5d2b4e0426550d288c8434590")]
		public string OfflineToken { get { return ConfigurationManager.AppSettings["OfflineToken"]; } }

		//	[Default("d125e41a182f7820df18bca26f1c8ad69e3049b47a9be59c3bb8f19ba09c1773")]
		public string OfflineHash { get { return ConfigurationManager.AppSettings["OfflineHash"]; } }

		//[Default("http://platform.shopyourway.com")]
		public Uri PlatformsiteUrl { get { return new Uri(ConfigurationManager.AppSettings["platform:syw-site-url"]); } }

		public Uri PlatformApiBaseUrl { get { return new Uri(ConfigurationManager.AppSettings["platform:api-url"]); } }

		public Uri PlatformSecureApiBaseUrl { get { return new Uri(ConfigurationManager.AppSettings["platform:secured-api-url"]); } }

	}
}
